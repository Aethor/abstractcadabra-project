# The Abstractcadabra Project

This project aims to classify sentences in scientific papers abstracts. possible classes are _BACKGROUND_, _OBJECTIVES_, _METHODS_, _RESULTS_, _CONCLUSIONS_ and _OTHERS_, and each sentences can belong in multiples categories.

This project was made to compete in the T-Brain AI-Cup 2019 "Thesis Tagging" contest, where it obtained the 51st place with 66.97 F1 score. It was later discovered that a bug in the implementation penalised learning, and a new score of around 70 F1 was easily obtained after correction.


## General concept

The main idea is to use a hierarchical model to classify sentences in abstracts. Specifically, 2 levels are involved : 

* sentence level
* global level

We first generate a representation of each sentence using a pretrained XLNet or BERT model from the _transformers_ library (our best result was achieved with XLNet). To do that, we encode the whole sentence using this model and take the first and last tokens embeddings as the sentence represention.

Then, we use those reprensentations as input of a bi-directional GRU, the intuition being that this GRU captures the relationships between sentences. We use the output of this GRU as input for a fully connected layer, and finally end our pipeline with a sigmoid as our activation function. 


## Detailed report

This repository contains a detailed report with in-depth explanations about the system. This report has been made before the bug correction, and therefore still references our old score of 66.97 F1. you can run the `make_report.sh` to generate the corresponding `report.pdf` file, or download it from [here](https://drive.google.com/open?id=1yXpPkRviuXfG6A0qUQQn782hfrFmdzLI)
