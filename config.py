import os
import json
from argparse import Namespace
from copy import copy


class Config:
    def __init__(self):
        self.defaults = json.loads(
            open(
                os.path.dirname(os.path.abspath(__file__)) + "/default-config.json"
            ).read()
        )
        self.config = copy(self.defaults)

    def load_config(self, namespace: Namespace):
        args_dict = vars(namespace)
        for key in self.config.keys():
            if key in args_dict:
                self.config[key] = args_dict[key]

    def __getitem__(self, key: str):
        return self.config[key]
