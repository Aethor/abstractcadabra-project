import csv
from typing import Generator, List, Dict, Tuple, Union, Optional
from random import shuffle

import torch
from transformers import XLNetTokenizer, BertTokenizer

from utils import flatten_seq


labels = {
    "BACKGROUND": 0,
    "OBJECTIVES": 1,
    "METHODS": 2,
    "RESULTS": 3,
    "CONCLUSIONS": 4,
    "OTHERS": 5,
}


def label_to_idx(label: str):
    return labels[label]


def idx_to_label(idx: int):
    return {v: k for k, v in labels.items()}[idx]


def tensor_to_labels(tensor: torch.Tensor, threshold: float) -> List[List[str]]:
    """
    :param tensor: (labels_nb)
    :param threshold: minimum value for an item of the tensor to be considered
        as a label
    :return: a list of list of labels as string
    """
    if tensor.shape != torch.Size([labels_nb()]):
        raise Exception("[error] wrong tensor shape : {}".format(tensor.shape))
    if threshold <= 0 or threshold > 1:
        raise Exception("[error] unusual threshold : {}".format(threshold))
    labels = []
    for i in range(tensor.shape[0]):
        if tensor[i].item() >= threshold:
            labels.append(idx_to_label(i))
    if len(labels) == 0:
        labels.append(idx_to_label(torch.max(tensor, 0)[1].item()))
    return labels


def labels_nb() -> int:
    return len(list(labels))


def weights(datas_list: List[dict]) -> List[float]:
    """
    :param datas_list: a dataset
    :return: A list of weight, one per label
    """
    occurences = [0] * labels_nb()
    for abstract in datas_list:
        for sentence in abstract["sentences"]:
            for label in sentence["labels"]:
                occurences[label_to_idx(label)] += 1
    max_weight = max(occurences)
    weights = [max_weight / occ for occ in occurences]
    return weights


def extract_datas(
    filename: str,
    train_test_ratio: Optional[float] = 0.75,
    datas_usage_ratio: float = 1.0,
    split_datas: bool = True,
    has_last_row: bool = True,
    is_shuffle: bool = True,
) -> (List[dict], Optional[List[dict]]):
    """extract datas from a file
    
    :param filename: path to the csv file containing raw datas
    :param train_test_ratio: len(train datas) / len(test) ratio, between 0 and 1
    :param datas_usage_ratio: percent of data to be used, between 0 and 1
    :param split_datas: 
    :param has_last_row:
    :param is_shuffle:
    :return: (train datas, test_datas)
    """
    if datas_usage_ratio < 0 or datas_usage_ratio > 1:
        raise Exception(
            "[error] datas_usage_ratio outside of correct range (value : {})".format(
                datas_usage_ratio
            )
        )

    f = open(filename)
    reader = csv.reader(f)

    datas = []
    for row in reader:
        cur_dict = {
            "id": row[0],
            "title": row[1],
            "authors": row[3].split("/"),
            "categories": row[4].split("/"),
            "date": row[5],
        }

        cur_dict["sentences"] = []
        sentences_nb = len(row[2].split("$$$"))
        for i, sent in enumerate(row[2].split("$$$")):
            cur_dict["sentences"].append(
                {
                    "text": sent,
                    "position": i + 1 // sentences_nb,
                    "labels": None,
                    "is_first": i == 0,
                    "is_last": i == sentences_nb - 1,
                }
            )

        if has_last_row:
            for i, labels in enumerate(row[6].split(" ")):
                try:
                    cur_dict["sentences"][i]["labels"] = [l for l in labels.split("/")]
                except Exception:
                    print("[debug] skipped a line")

        datas.append(cur_dict)

    del datas[0]
    datas = datas[: int(datas_usage_ratio * len(datas))]

    f.close()

    if split_datas:
        train_test_limit = int(len(datas) * train_test_ratio)
        train_datas, test_datas = (datas[:train_test_limit], datas[train_test_limit:])
        if is_shuffle:
            shuffle(train_datas)
            shuffle(test_datas)
        return train_datas, test_datas
    else:
        if is_shuffle:
            shuffle(datas)
        return datas, None


def write_datas(filename: str, datas: List[dict], is_submission: bool = False):
    f = open(filename, "w")
    writer = csv.writer(f, delimiter=",", quotechar='"')

    if is_submission:
        writer.writerow(
            [
                "order_id",
                "BACKGROUND",
                "OBJECTIVES",
                "METHODS",
                "RESULTS",
                "CONCLUSIONS",
                "OTHERS",
            ]
        )
        for data in datas:
            for i, sentence in enumerate(data["sentences"]):
                sent_id = (
                    data["id"] + "_S" + (str(0) * (3 - len(str(i + 1)))) + str(i + 1)
                )
                if sentence["labels"] is None:
                    print("[warning] sentence had no label, assigning BACKGROUND")
                    writer.writerow([sent_id, 1, 0, 0, 0, 0, 0])
                    continue
                writer.writerow(
                    [
                        sent_id,
                        1 if "BACKGROUND" in sentence["labels"] else 0,
                        1 if "OBJECTIVES" in sentence["labels"] else 0,
                        1 if "METHODS" in sentence["labels"] else 0,
                        1 if "RESULTS" in sentence["labels"] else 0,
                        1 if "CONCLUSIONS" in sentence["labels"] else 0,
                        1 if "OTHERS" in sentence["labels"] else 0,
                    ]
                )

        f.close()
        return

    writer.writerow(
        ["Id", "Title", "Abstract", "Authors", "Categories", "Created Date"]
    )
    for data in datas:
        texts = [s["text"] for s in data["sentences"]]
        labels = ["/".join(s["labels"]) for s in data["sentences"]]
        writer.writerow(
            [
                data["id"],
                data["title"],
                "$$$".join(texts),
                "/".join(data["authors"]),
                "/".join(data["categories"]),
                data["date"],
                " ".join(labels),
            ]
        )
    f.close()


def prepare_datas(
    abstracts: List[List[List[str]]],
    labels: List[List[Optional[List[str]]]],
    tokenizer: Union[XLNetTokenizer, BertTokenizer],
    device: torch.device,
) -> (List[torch.Tensor], List[torch.Tensor], List[Optional[torch.Tensor]]):
    """transform tokenized sentences and labels into tensors

    :param sents: A list of abstracts, each being a list of sentences,
        each being a list of tokens
    :param labels: A list of labels for each sentence of each abstract
    :param tokenizer: a XLNetTokenizer or a BertTokenizer. This function
        automatically handle both cases and insert correct special tokens
        when tokenizing
    :param device:
    :return: (X, attention_mask, y)
        X, attention_mask :
            list(tensors(var abstract_len, var max_sent_len))(len(abstracts))
        y :
            list(tensors(var abstract_len, labels_nb))(len(abstracts))
    """
    X = []
    attention_mask = []
    y = []

    for i, abstract in enumerate(abstracts):
        X.append([])
        attention_mask.append([])
        y.append([])
        max_sent_len = max([len(sent) for sent in abstract])

        for j, sent in enumerate(abstract):

            encoded_tokens = tokenizer.encode(sent)
            added_tokens_nb = len(encoded_tokens) - len(sent)

            padding_len = 0
            if len(encoded_tokens) != max_sent_len + added_tokens_nb:
                padding_len = max_sent_len - len(encoded_tokens) + added_tokens_nb

                if isinstance(tokenizer, XLNetTokenizer):
                    encoded_tokens += tokenizer.encode(
                        ["<pad>"] * padding_len, add_special_tokens=False
                    )
                elif isinstance(tokenizer, BertTokenizer):
                    encoded_tokens += tokenizer.encode(
                        ["[PAD]"] * padding_len, add_special_tokens=False
                    )
                else:
                    raise Exception(
                        "[error] unknown tokenizer type : {}".format(tokenizer)
                    )

            X[i].append(encoded_tokens)
            attention_mask[i].append(
                [1] * (len(encoded_tokens) - padding_len) + [0] * padding_len
            )
            if labels[i][j] is None:
                y[i].append(None)
            else:
                y[i].append([0] * labels_nb())
                for label in labels[i][j]:
                    y[i][j][label_to_idx(label)] = 1

    for i in range(len(abstracts)):
        X[i] = torch.tensor(X[i]).to(device)
        attention_mask[i] = torch.tensor(attention_mask[i]).to(device)
        if not None in y[i]:
            y[i] = torch.tensor(y[i], dtype=torch.float).to(device)

    return X, attention_mask, y


def batches_nb(datas_list: List[dict], batch_size: int) -> int:
    return len(datas_list) // batch_size


def batches(
    datas_list: List[dict],
    tokenizer: Union[XLNetTokenizer, BertTokenizer],
    batch_size: int,
    device: torch.device,
) -> Generator[Tuple[torch.Tensor], None, None]:
    """See prepare_datas documentation for details
    """
    for i in range(batches_nb(datas_list, batch_size)):
        abstract_dicts = datas_list[i * batch_size : (i + 1) * batch_size]
        abstracts = []
        y = []
        for abstract_dict in abstract_dicts:
            sent_list = abstract_dict["sentences"]
            abstracts.append([tokenizer.tokenize(sent["text"]) for sent in sent_list])
            y.append([sent["labels"] for sent in sent_list])
        X, attention_mask, y = prepare_datas(abstracts, y, tokenizer, device)
        yield X, attention_mask, y
