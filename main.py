import os

os.environ["TF_CPP_MIN_LOG_LEVEL"] = "3"


import argparse
import json
from datetime import datetime
from typing import List, Union, Callable
from shutil import copyfile

import torch
import matplotlib.pyplot as plt
from transformers import XLNetTokenizer, BertTokenizer, AdamW
from tqdm.auto import tqdm

from datas import (
    extract_datas,
    write_datas,
    batches,
    batches_nb,
    labels,
    labels_nb,
    tensor_to_labels,
    weights,
)
from model import SentenceClassifierModel
from utils import flatten_seq
from config import Config


def batch_loss_fn(
    pred: torch.Tensor,
    target: List[torch.Tensor],
    loss_fn: Callable[[torch.Tensor, torch.Tensor], torch.Tensor],
) -> torch.Tensor:
    """
    :param pred:   (batch_size, max_sents_nb, labels_nb)
    :param target: list(tensor(var abstract_len, labels_nb))(batch_size)
    :return:       (1)
    """
    sum_loss = torch.tensor(0, dtype=torch.float).to(pred.device)
    for abstract_idx in range(len(target)):
        max_sent_len = target[abstract_idx].shape[0]
        sum_loss += loss_fn(pred[abstract_idx, :max_sent_len, :], target[abstract_idx])
    return torch.mean(sum_loss, 0)


def train_(
    model: SentenceClassifierModel,
    optimizer,
    loss_fn: Callable[[torch.Tensor, torch.Tensor], torch.Tensor],
    device: torch.device,
    tokenizer: XLNetTokenizer,
    train_datas: List[dict],
    val_datas: List[dict],
    epoch_nbs: int,
    batch_size: int,
) -> (SentenceClassifierModel, List[float], List[float]):
    model.to(device)

    mean_train_losses = []
    val_f1s = []

    for epoch in range(epoch_nbs):
        batches_progress = tqdm(
            batches(train_datas, tokenizer, batch_size, device),
            total=batches_nb(train_datas, batch_size),
        )

        train_losses = []

        for X, attention_mask, y in batches_progress:
            model.train()
            optimizer.zero_grad()

            hidden_state = model.init_hidden_state(batch_size).to(device)
            pred, _, _ = model(X, attention_mask, hidden_state)
            loss = batch_loss_fn(pred, y, loss_fn)

            loss.backward()
            # TODO: hardcoded clipping
            torch.nn.utils.clip_grad_norm_(model.parameters(), 0.5)

            optimizer.step()

            batches_progress.set_description(
                "[e:{}][l:{:0.4f}]".format(epoch + 1, loss.item())
            )
            train_losses.append(loss.item())

        mean_loss = sum(train_losses) / len(train_losses)
        mean_train_losses.append(mean_loss)
        _, _, f1 = score(model, device, tokenizer, val_datas, batch_size)
        val_f1s.append(f1)
        tqdm.write(
            "[mean loss : {:0.4f}][validation f1 : {:0.4f}]".format(mean_loss, f1)
        )

    return model, mean_train_losses, val_f1s


def predict_labels(
    model: SentenceClassifierModel,
    device: torch.device,
    X: List[torch.Tensor],
    attention_mask: List[torch.Tensor],
) -> List[torch.Tensor]:
    """ Predict sentences label for a batch of abstracts

    :param model: the model to use for prediction
    :param device:
    :param X: list(tensors(abstract_len, max_sent_len))(batch_size)
    :param attention_mask: list(tensors(abstract_len, max_sent_len))(batch_size)
    :return: list(tensors(abstract_len, labels_nb))(batch_size)
    """
    model.eval()
    model.to(device)

    labels: List[torch.Tensor] = list()
    with torch.no_grad():
        batch_size = len(X)
        hidden_state = model.init_hidden_state(batch_size).to(device)
        raw_preds, _, lengths = model(X, attention_mask, hidden_state)
        preds = torch.sigmoid(raw_preds)
        for i in range(len(X)):
            labels.append(preds[i, : lengths[i].item(), :])

    return labels


def score(
    model: SentenceClassifierModel,
    device: torch.device,
    tokenizer: Union[XLNetTokenizer, BertTokenizer],
    datas: List[dict],
    batch_size: int,
) -> (float, float, float):
    true_positives = 0
    false_positives = 0
    false_negatives = 0

    model.to(device)
    model.eval()
    batches_progress = tqdm(
        batches(datas, tokenizer, batch_size, device),
        total=batches_nb(datas, batch_size),
    )

    for X, attention_mask, y in batches_progress:

        label_preds = predict_labels(model, device, X, attention_mask)

        # (abstract_len, labels_nb)
        for label_pred, label_truth in zip(label_preds, y):

            abstract_len = label_pred.shape[0]
            labels_nb = label_pred.shape[1]
            for i in range(abstract_len):
                for j in range(labels_nb):
                    pred = 1 if label_pred[i][j].item() > 0.5 else 0
                    truth = label_truth[i][j].item()

                    if truth == 1 and pred == 1:
                        true_positives += 1
                    elif truth == 1 and pred == 0:
                        false_negatives += 1
                    elif truth == 0 and pred == 1:
                        false_positives += 1

    precision, recall, f1 = (0, 0, 0)
    if true_positives + false_positives != 0:
        precision = true_positives / (true_positives + false_positives)
    if true_positives + false_negatives != 0:
        recall = true_positives / (true_positives + false_negatives)
    if precision + recall != 0:
        f1 = 2 * (precision * recall) / (precision + recall)

    return precision, recall, f1


if __name__ == "__main__":

    config = Config()
    arg_parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    arg_parser.add_argument(
        "-t", "--train", action="store_true", help="If specified, train a model"
    )
    arg_parser.add_argument(
        "-s",
        "--score",
        type=str,
        default=None,
        help="If specified, score the specified model",
    )
    arg_parser.add_argument(
        "-u",
        "--submit",
        type=str,
        default=None,
        help="If specified, generate a datas/submission.csv file using the specified model",
    )
    arg_parser.add_argument(
        "-tm",
        "--transformer-model",
        type=str,
        default=config.defaults["transformer_model"],
        help="'xlnet' or 'bert'",
    )
    arg_parser.add_argument(
        "-dur",
        "--datas-usage-ratio",
        type=float,
        default=config.defaults["datas_usage_ratio"],
        help="data usage ratio, between 0 and 1",
    )
    arg_parser.add_argument(
        "-ttr",
        "--train-test-ratio",
        type=float,
        default=config.defaults["train_test_ratio"],
        help="train test ratio, between 0 and 1",
    )
    arg_parser.add_argument(
        "-en",
        "--epochs-nb",
        type=int,
        default=config.defaults["epochs_nb"],
        help="Number of epochs",
    )
    arg_parser.add_argument(
        "-bz",
        "--batch-size",
        type=int,
        default=config.defaults["batch_size"],
        help="Size of batches",
    )
    arg_parser.add_argument(
        "-sbz",
        "--sent-batch-size",
        type=int,
        default=config.defaults["sent_batch_size"],
        help="Batch size at the sentence level",
    )
    arg_parser.add_argument(
        "-ftln",
        "--frozen-transformer-layers-nb",
        type=int,
        default=config.defaults["frozen_transformer_layers_nb"],
        help="Number of transformer layer to freeze [0-12]",
    )
    arg_parser.add_argument(
        "-ghs",
        "--gru-hidden-size",
        type=int,
        default=config.defaults["gru_hidden_size"],
        help="Hidden size of the sequence-level GRU",
    )
    arg_parser.add_argument(
        "-gln",
        "--gru-layers-nb",
        type=int,
        default=config.defaults["gru_layers_nb"],
        help="Number of layer of the sequence-level GRU",
    )
    arg_parser.add_argument(
        "-gd",
        "--gru-dropout",
        type=float,
        default=config.defaults["gru_dropout"],
        help="Dropout of the sequence-level GRU",
    )
    arg_parser.add_argument(
        "-tlr",
        "--transformer-learning-rate",
        type=float,
        default=config.defaults["transformer_learning_rate"],
        help="Transformer learning rate",
    )
    arg_parser.add_argument(
        "-dlr",
        "--default-learning-rate",
        type=float,
        default=config.defaults["default_learning_rate"],
        help="Default learning rate",
    )
    args = arg_parser.parse_args()
    config.load_config(args)

    if args.train or args.score:
        train_datas, test_datas = extract_datas(
            "datas/trainset.csv",
            datas_usage_ratio=config["datas_usage_ratio"],
            train_test_ratio=config["train_test_ratio"],
        )

    device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

    if config["transformer_model"] == "xlnet":
        tokenizer = XLNetTokenizer.from_pretrained("xlnet-base-cased")
    elif config["transformer_model"] == "bert":
        tokenizer = BertTokenizer.from_pretrained("bert-base-cased")
    else:
        print(
            "[error] unknown transformer model : {}".format(config["transformer_model"])
        )
        exit()

    model = None

    if args.train:
        training_id = datetime.now().strftime("%Y-%m-%d.%H:%M:%S")

        print(
            "starting training {} with configuration :\n{}".format(
                training_id, json.dumps(config.config, indent=4)
            )
        )

        model = SentenceClassifierModel(
            config["transformer_model"],
            config["sent_batch_size"],
            config["gru_hidden_size"],
            config["gru_layers_nb"],
            config["gru_dropout"],
        )

        # BERT : list(model.transformer.encoder.children())[0]
        # XLNet : list(model.transformer.layer.children())
        if config["transformer_model"] == "xlnet":
            transformer_layers = list(model.transformer.layer.children())
        elif config["transformer_model"] == "bert":
            transformer_layers = list(model.transformer.encoder.children())[0]
        else:
            raise Exception("unknown transformer type")
        for i, layer in enumerate(transformer_layers):
            if i < config["frozen_transformer_layers_nb"]:
                print(f"[info] freezing layer {i} of transformer")
                for parameter in layer.parameters():
                    parameter.requires_grad = False

        transformer_parameters = [
            p for p in model.transformer.parameters() if p.requires_grad
        ]
        optimizer = AdamW(
            [
                {
                    "params": transformer_parameters,
                    "lr": config["transformer_learning_rate"],
                },
                {"params": model.sentence_classifier.parameters()},
            ],
            lr=config["default_learning_rate"],
        )

        class_weights = torch.tensor(weights(train_datas)).to(device)
        print(f"[debug] class weights : {class_weights}")
        loss = torch.nn.BCEWithLogitsLoss(pos_weight=class_weights)

        model, losses, val_f1s = train_(
            model,
            optimizer,
            loss,
            device,
            tokenizer,
            train_datas,
            test_datas,  # TODO: also generate validation datas or you will overfit later ;)
            config["epochs_nb"],
            config["batch_size"],
        )

        training_dir = "out/" + training_id
        os.mkdir(training_dir)
        torch.save(model.state_dict(), "out/" + training_id + "/model.pth")

        plt.subplot(2, 1, 1)
        plt.plot(losses)
        plt.ylabel("losses")
        plt.xlabel("epochs")
        plt.subplot(2, 1, 2)
        plt.plot(val_f1s)
        plt.ylabel("validation f1")
        plt.xlabel("epochs")
        plt.savefig(training_dir + "/losses.png")

        for datas in [train_datas, test_datas]:
            precision, recall, f1 = score(
                model, device, tokenizer, datas, config["batch_size"]
            )
            print(
                "scores on {} :\nprecision : {:0.4f}\nrecall : {:0.4f}\nf1 : {:0.4f}".format(
                    "train datas" if datas is train_datas else "test datas",
                    precision,
                    recall,
                    f1,
                )
            )

        open(training_dir + "/scores.json", "w").write(
            json.dumps({"precision": precision, "recall": recall, "f1": f1}, indent=4)
        )
        open(training_dir + "/config.json", "w").write(
            json.dumps(config.config, indent=4)
        )

    if args.score:
        if model == None:
            model = SentenceClassifierModel(
                config["transformer_model"],
                config["sent_batch_size"],
                config["gru_hidden_size"],
                config["gru_layers_nb"],
                config["gru_dropout"],
            )
            model.load_state_dict(
                torch.load(args.score, map_location=device), strict=False
            )

        for datas in [train_datas, test_datas]:
            precision, recall, f1 = score(
                model, device, tokenizer, datas, config["batch_size"]
            )
            print(
                "scores on {} :\nprecision : {}\nrecall : {}\nf1 : {}".format(
                    "train datas" if datas is train_datas else "test datas",
                    precision,
                    recall,
                    f1,
                )
            )

    if args.submit:
        if model == None:
            model = SentenceClassifierModel(
                config["transformer_model"],
                config["sent_batch_size"],
                config["gru_hidden_size"],
                config["gru_layers_nb"],
                config["gru_dropout"],
            )
            model.load_state_dict(
                torch.load(args.submit, map_location=device), strict=False
            )

        print("loading datas...")
        if config["datas_usage_ratio"] != 1:
            print(
                f"[warning] datas_usage_ratio is different from 1 ({config['datas_usage_ratio']}), submission may be incorrect"
            )
        datas = extract_datas(
            "datas/task1_public_testset.csv",
            None,
            config["datas_usage_ratio"],
            False,
            False,
            False,
        )[0]

        batches_progress = tqdm(
            batches(datas, tokenizer, config["batch_size"], device),
            total=batches_nb(datas, config["batch_size"]),
        )
        datas_i = 0
        for X, attention_mask, _ in batches_progress:

            labels_preds = predict_labels(model, device, X, attention_mask)

            for label_pred in labels_preds:
                abstract_len, labels_nb = label_pred.shape[0], label_pred.shape[1]
                for i in range(abstract_len):
                    datas[datas_i]["sentences"][i]["labels"] = tensor_to_labels(
                        label_pred[i], 0.5
                    )
            datas_i += config["batch_size"]

        write_datas("datas/submission.csv", datas, is_submission=True)
        print("submission ready !")
