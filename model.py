from typing import List

import torch
from torch.nn.utils.rnn import PackedSequence, pad_packed_sequence, pack_sequence
from torch.nn.modules import Linear
from transformers import XLNetModel, BertModel

from datas import labels_nb


class SentenceEncoder(torch.nn.Module):
    def __init__(self, transformer_model: str):
        super().__init__()
        if transformer_model == "xlnet":
            self.transformer = XLNetModel.from_pretrained("xlnet-base-cased")
        elif transformer_model == "bert":
            self.transformer = BertModel.from_pretrained("bert-base-cased")
        else:
            raise Exception(
                "[error] unknown transformer model : {}".format(transformer_model)
            )
        self.hidden_size = self.transformer.config.hidden_size

    def forward(self, sent: torch.Tensor, attention_mask: torch.Tensor) -> torch.Tensor:
        """
        :param sent:           (sent_batch_size, sent_size)
        :param attention_mask: (sent_batch_size, sent_size)
        :return:               (sent_batch_size, 2 * hidden_size)
        """
        sent_batch_size = sent.shape[0]

        # (sent_batch_size, seq_size, hidden_size)
        fully_encoded_sent = self.transformer(sent, attention_mask=attention_mask)[0]

        encoded_sent = torch.flatten(
            torch.cat((fully_encoded_sent[:, 0, :], fully_encoded_sent[:, -1, :]), 1),
            start_dim=1,
        )
        return encoded_sent


class SentenceClassifier(torch.nn.Module):
    def __init__(
        self, hidden_size: int, gru_hidden_size: int, layers_nb: int, dropout: float
    ):
        """SentenceClassifier constructor

        :param transformer_model: "bert" or "xlnet"
        """
        super().__init__()
        self.gru_hidden_size = gru_hidden_size
        self.layers_nb = layers_nb
        self.directions_nb = 2

        self.gru = torch.nn.GRU(
            2 * hidden_size,
            self.gru_hidden_size,
            layers_nb,
            batch_first=True,
            dropout=dropout,
            bidirectional=True,
        )
        self.linear = Linear(self.gru_hidden_size * self.directions_nb, labels_nb())

    def init_hidden_state(self, batch_size: int) -> torch.Tensor:
        """
        :param batch_size:
        :return: (layers_nb * directions_nb, batch_size, gru_hidden_size)
        """
        return torch.zeros(
            self.layers_nb * self.directions_nb, batch_size, self.gru_hidden_size
        )

    def forward(self, seqs: PackedSequence, hidden_state: torch.Tensor) -> torch.Tensor:
        """
        :param seqs: packed_sequence(batch_size, var sents_nb, 2 * hidden_size)
        :param hidden_state: (batch_size, layers_nb * directions_nb, gru_hidden_size)
        :return: out, new_hidden_state, lengths
            out :              (batch_size, max_sents_nb, labels_nb)
            new_hidden_state : (batch_size, layers_nb * directions_nb, gru_hidden_size)
            lengths :          (batch_size)
        """
        gru_out, new_hidden_state = self.gru(seqs, hidden_state)
        gru_out, lengths = pad_packed_sequence(gru_out, batch_first=True)
        out = self.linear(gru_out)
        return out, new_hidden_state, lengths


class SentenceClassifierModel(torch.nn.Module):
    def __init__(
        self,
        transformer_model: str,
        sent_batch_size: int,
        gru_hidden_size: int,
        gru_layers_nb: int,
        gru_dropout: float,
    ):
        super().__init__()
        self.sent_batch_size = sent_batch_size

        self.sentence_encoder = SentenceEncoder(transformer_model)
        self.hidden_size = self.sentence_encoder.hidden_size
        self.transformer = self.sentence_encoder.transformer

        self.sentence_classifier = SentenceClassifier(
            self.hidden_size, gru_hidden_size, gru_layers_nb, gru_dropout
        )

    def init_hidden_state(self, batch_size: int) -> torch.Tensor:
        return self.sentence_classifier.init_hidden_state(batch_size)

    def forward(
        self,
        abstracts: List[torch.Tensor],
        attention_masks: List[torch.Tensor],
        hidden_state: torch.Tensor,
    ) -> torch.Tensor:
        """
        :param abstracts: list(tensor(var abstract_len, var max_sent_len))(batch_size)
        :param attention_masks: list(tensor(var abstract_len, var max_sent_len))(batch_size)
        :param hidden_states: (batch_size, max_sents_nb, labels_nb)
        :return: out, new_hidden_state, lengths (see SentenceClassifier() doc for more infos)
        """
        seqs: List[torch.Tensor] = list()
        for abstract, attention_mask in zip(abstracts, attention_masks):

            abstract_len = abstract.shape[0]
            sent_batch_nb = abstract_len // self.sent_batch_size

            abstract_len = abstract.shape[0]
            seq = torch.zeros(abstract_len, 2 * self.hidden_size).to(abstract.device)

            for sent_batch_idx in range(sent_batch_nb):
                from_idx = sent_batch_idx * self.sent_batch_size
                to_idx = (sent_batch_idx + 1) * self.sent_batch_size
                encoded_sent_batch = self.sentence_encoder(
                    abstract[from_idx:to_idx, :], attention_mask[from_idx:to_idx, :]
                )
                seq[from_idx:to_idx, :] = encoded_sent_batch

            seqs.append(seq)

        return self.sentence_classifier(
            pack_sequence(seqs, enforce_sorted=False), hidden_state
        )
