---
title: "__Thesis Abstract Sentences Tagging using Deep Hierarchical Representations__"
author: | 
	```{=latex}
	Arthur Amalvy \\ \texttt{108522605} \\ \texttt{arthur.amalvy@utbm.fr}
	```
---

\newpage

# Introduction

With so much scientific papers being published every year, searching for information is becoming increasingly harder as time passes. A system able to meaningfully retrieve informations in this sea of papers would surely prove to be very valuable. The __T-Brain AICup 2019 Thesis Tagging__ contest is a step in that direction. In this contest, participants must create a system able to tag each sentence of a thesis abstract with the following tags : _BACKGROUND_, _OBJECTIVES_, _METHODS_, _RESULTS_, _CONCLUSIONS_ and _OTHERS_. To do so, we propose a hierarchical representation system __[1]__. We release all our code freely under the MIT license\footnote{https://gitlab.com/Aethor/abstractcadabra-project}.


# Dataset

The training set is composed of 7000 examples, each of them coming with the following fields :

* ID : an unique ID identifying the abstract
* Title : The title of the scientific paper the abstract is from
* Abstract : The text of the abstract, splitted by sentences
* Authors : The list of authors of the scientific paper
* Categories : The list of scientific categories the paper belongs to (see [__arXiv categories__](https://arxiv.org/help/prep#subj))
* Date : The date the paper was published
* Tags : The list of tags of each abstract sentence

For the sake of simplicity, we decided to use only abstract texts in our system.


# Method

Inspired by work on Dialogue Act Tagging\footnote{Dialogue Act Tagging is a task where, given a sequence of utterance, a system must assign a dialogue function to each of those utterance (such as \texttt{STATEMENT}, \texttt{ANSWER}, etc...)} __[1]__, we aim to create a _hierarchical system_. This system has two levels :

* Sentence level : This level aims to capture the general essence of sentences
* Global level : This level aims to capture relationships between sentences


## Sentence Level

At the sentence level, we want to generate an independant representation of each sentence of an abstract. To do so, we experimented with both XLNet __[2]__ and BERT __[3]__ to generate a meaningful representation. For both of these models, we encode the whole sentence and take the concatenation of the first and last token representation as the sentence representation.


## Global Level

The global level of our system is trying to represent relationships between sentences. This is to take into account our intuition that some sentence classes occur more at certain points in the abstract (a `CONCLUSION` sentence is usually at the end), and that some sentences classes positions are influenced by others (a `CONCLUSION` sentence generally follows a `RESULT` one).

To model those relationships, we use a bi-directional GRU in a sequence-to-sequence manner. This GRU takes as input an abstract as a sequence of encoded sentences (as previously described), and generate a series of representations. Those representations are then used as input of a fully connected neural network layer, followed by a sigmoid activation function. For each sentence, this produces the probability of each class.


## Loss function

We use a weighted binary cross entropy loss as our loss function. given an abstract with $N$ sentences, considering we have C possible classes, the loss of a prediction can be computed as :

\begin{equation}
L = \frac{1}{N C} \sum^{n, c} l_{n,c}
\end{equation}

where :

\begin{equation}
l_{n,c} = w_c y^\ast_{n, c} log(\hat{y}_{n,c}) + (1 - y^\ast_{n,c}) log(1 - \hat{y}_{n,c})
\end{equation}

$w_{c}$ being the weight associated with class $c$. We compute $w_c$ using the following formula :

\begin{equation}
w_c = \frac{o(w_m)}{o(w_c)}
\end{equation}

where $m$ is the most frequent class in the dataset, and $o(w_c)$ is the number of appearance of class $c$ in the dataset.


# Experimental Setup

We implement our model using PyTorch. To allow for quick, easy and reproducible experiments, we use json files to specify model parameters. The following example is the configuration that was used to obtain our best result :

```json
{
    "transformer_model": "xlnet",
    "datas_usage_ratio": 1,
    "train_test_ratio": 0.9,
    "epochs_nb": 7,
    "batch_size": 1,
    "sent_batch_size": 3,
    "frozen_transformer_layers_nb": 8,
    "gru_hidden_size": 512,
    "gru_layers_nb": 1,
    "gru_dropout": 0.0,
    "transformer_learning_rate": 1e-5,
    "default_learning_rate": 5e-5
}
```

Most of these parameters are straightforward to understand, but some of them deserve more attention :

* The `frozen_transformer_layers_nb` parameter controls how many layers of BERT or XLNet are frozen at training time (starting from layers closer to the input, we can freeze up to 12 layers). Freezing those layers was done because we observed that our model would dramatically overfit otherwise.
* The `transformer_learning_rate` controls the learning rate of the BERT / XLNet layers, while the `default_learning_rate` controls the learning rate of the GRU and final fully connected layer. Setting them to different numbers was inspired by _Joshi et al., 2019_ __[4]__, and was found experimentally to give better results.

We use the AdamW optimizer implementation in PyTorch with default parameters(except for learning rate as stated above).

To prevent exploding gradients problems, we clip the gradient when its norm is greater than an arbitrary threshold of 0.5.

When training, we used 90% of the data as training set, while the remaining 10% was used as a validation set (for example, this was useful to see the evolution of the validation F1 score during training).

![Training loss and validation F1 during training](./assets/training.png)

\newpage


# Results

|      Results       | Precision |   Recall  |    F1   |
|:------------------:|:---------:|:---------:|:-------:|
| Online Leaderboard | _unknown_ | _unknown_ |  66.97  |
| Our Validation Set |    68.35  |    62.70  |  65.40  |

![Leaderboard screenshot of our result](./assets/screenshot.png)

We obtain 66.97 F1-score, getting us the 51st rank of the leaderboard. Overall, we feel like this is a decent result, but with a lot of room for improvements. For example, we could have trained a different model for each tag, tried to optimize our hyperparameters using bayesian methods, used other fields from the supplied datas, used a transformer instead of a GRU...


# Conclusion

We propose a deep hierarchical representation model to tackle the task of thesis abstract sentences tagging, and achieve a score of 66.97 F1 on the T-Brain competition dataset, getting us the 51st place of the leaderboard.



# References

* __[1]__ _Tran Q. H., Zukerman I., Haffari G. A Hierarchical Neural Model for Learning Seqences of Dialogue Acts. 2017._
* __[2]__ _Yang Z., Dai Z., Yang Y., Carbonell J., Salakhutdiniv R, Le Q. V. XLNet: Generalized Autoregressive Pretraining for Language Understanding. 2019._
* __[3]__ _Devlin J., Chang M-W., Lee K., Toutanova K. BERT: Pre-training of Deep Bidirectional Transformer for Language Understanding_
* __[4]__ _Joshi M., Levy O., Weld D. S., Zettlemoyer L. BERT for Coreference Resolution: Baselines and Analysis. 2019_
