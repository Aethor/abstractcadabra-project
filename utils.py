from typing import Iterable


def flatten_seq(seq: Iterable[Iterable]):
    return [item for sublist in seq for item in sublist]
